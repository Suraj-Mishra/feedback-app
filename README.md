# The feedback App

An interviewer's feedback application for interviewees.
---

Using this application, the interviewee can provide feedback for the interview. The interviewee can select how the interview went, how the interviewers were and they can write a comment in their own words.

---

This project is built in React using create-react-app.

Use `npm run` to start the app on your local server.

To run the production build, use `npm run build`.

The feedback, including the comments, is stored on firebase.

# How it looks -

![Peek_2020-06-23_14-04](/uploads/75e4531fdd4558bc23f4bec0ef4853dc/Peek_2020-06-23_14-04.gif)
